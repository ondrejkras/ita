const express = require('express');
const router = express.Router();
const jwt = require('jwt-simple');

router.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
});

router.get('/login', (req, res) => {
    console.log('login request');
    const payload = {user: 'vondrc', valid: Date.now() + (10 * 60 * 1000)};
    const secret = 'xxx';
    const token = jwt.encode(payload, secret);
    res.json(token);
});

module.exports = router;