const express = require('express');
const router = express.Router();
const RecordService = require('../service/record.service');


router.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
});

router.get('/', (req, res) => {
    RecordService.getAllRecords(req, function (err, record) {
        if (err) {
            console.error(err.stack);
            return res.status(404).send('Something is wrong!');
        }
        res.json(record);
    });
});

router.get('/name', (req, res) => {
    RecordService.getRecordByName(req, req.query.name, function (err, record) {
        if (err) {
            console.error(err.stack);
            return res.status(404).send('Something is wrong!');
        }
        res.json(record);
    });
});

router.get('/:id', (req, res) => {
    RecordService.getRecordById(req, req.params.id, function (err, record) {
        if (err) {
            console.error(err.stack);
            return res.status(404).send('Something is wrong!');
        }
        res.json(record);
    });
});

router.put('/:id', (req, res) => {
    RecordService.updateRecord(req, req.params.id, req.body, {upsert: true}, function (err, record) {
        if (err) {
            console.error(err.stack);
            return res.status(404).send('Something is wrong!');
        }
        res.json(record);
    });
});

router.post('/create', function (req, res) {
    RecordService.createRecord(req, req.body, function (err, genre) {
        if (err) {
            console.error(err.stack);
            return res.status(404).send('Something is wrong!');
        }
        res.json(genre);
    });
});

module.exports = router;
