const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const recordsRutes = require('./routes/records');
const loginRoute = require('./routes/login');


const app = express();
mongoose.connect('mongodb://localhost/sport');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/api/record', recordsRutes);
app.use('/api', loginRoute)


mongoose.connection.on('error', (error) => console.error('Unable to connect to MongoDB instance.', error));
mongoose.connection.on('open', () => {
    console.log('MongoDB connected');
    app.listen(3000, () => console.log('Server started on port 3000'));
});
