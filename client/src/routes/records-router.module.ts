import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {RecordComponent} from "../record/record.component";
import {RecordAddComponent} from '../record/add-record/record-add.component';
import {LoginComponent} from "../app/login/login.component";
import {AuthGuard} from "./guards/auth-guard";
import {ChartComponent} from "../charts/charts.component";


const recordsRoutes: Routes = [
    {
        path: 'records',
        canActivateChild: [AuthGuard],
        children: [
            {
                path: '',
                component: RecordComponent
            },
            {
                path: 'add',
                component: RecordAddComponent
            }
        ]
    },
    {path: 'login', component: LoginComponent},
    {path: 'charts', component: ChartComponent, canActivate: [AuthGuard]},
    {path: '**', redirectTo: 'login'}
];

@NgModule({
    imports: [
        RouterModule.forChild(recordsRoutes)
    ],
    declarations: [],
    providers: [AuthGuard],
    exports: [RouterModule]
})
export class RecordsRoutesModule {

}