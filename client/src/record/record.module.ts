import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {HttpModule, JsonpModule} from "@angular/http";

import {RecordComponent} from "./record.component";
import {RecordAddComponent} from "./add-record/record-add.component";
import {DurationPipe} from "../pipes/custom.pipes";
import {TypeaheadModule} from "ngx-bootstrap";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        TypeaheadModule.forRoot()
    ],
    declarations: [
        RecordComponent,
        RecordAddComponent,
        DurationPipe
    ],
    providers: [],
    exports: [RecordComponent]
})
export class RecordModule {
}