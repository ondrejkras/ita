import {NgModule} from "@angular/core";
import {ChartComponent} from "./charts.component";
import {ChartService} from "./services/chart.service";
import {ChartModule} from 'angular2-highcharts';


@NgModule({
    imports: [ChartModule.forRoot(require('highcharts'))],
    declarations: [ChartComponent],
    providers: [ChartService],
    exports: [ChartComponent]
})

export class ChartsModule {
}