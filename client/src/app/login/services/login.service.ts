import {Injectable} from '@angular/core';
import HttpService from "../../../services/http.service";
const jwt = require('jwt-simple');


@Injectable()
export class LoginService {

    constructor(private httpService: HttpService) {
    }

    private loginUrl = 'api/login';

    logInUser() {
        return this.httpService.get(this.loginUrl)
            .map(data => {
                let decoded = jwt.decode(data, "xxx", true);
                localStorage.setItem('token', JSON.stringify(decoded));
                return "User logged!";
            })
            .catch(err => {
                throw "Login failed!"
            });
    }
}