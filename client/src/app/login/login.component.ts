import {Component} from "@angular/core";
import {LoginService} from "./services/login.service";

@Component({
    selector: 'login-comp',
    templateUrl: './login.component.html'
})
export class LoginComponent {

    userMessage: string;
    userError: string;

    constructor(private loginService: LoginService) {
    }

    logIn() {
        this.loginService.logInUser().subscribe(
            response => {
                this.userMessage = <any>response;
                this.userError = '';
            },
            error => {
                this.userError = <any>error;
                this.userMessage = ''
            })
    };
}
