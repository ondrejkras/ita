import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {HttpModule, JsonpModule} from "@angular/http";

import {AppComponent} from "./app.component";
import {RecordModule} from "../record/record.module";
import {RecordsRoutesModule} from "../routes/records-router.module";
import {LoginComponent} from "./login/login.component";
import {RecordService} from "../record/services/record.service";

import {RouterModule, Routes} from "@angular/router";
import {LoginService} from "./login/services/login.service";
import HttpService from "../services/http.service";
import {ChartsModule} from "../charts/charts.module";

const appRoutes: Routes = [];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        RecordModule,
        RecordsRoutesModule,
        RouterModule.forRoot(appRoutes),
        ChartsModule
    ],
    declarations: [
        AppComponent, LoginComponent
    ],
    providers: [RecordService, LoginService, HttpService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
